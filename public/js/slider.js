$(function(){
    if ($('.slider').length) {
        var slideWidth = 740;
        var slider = $('.slider');
        slider.append('<a class="prev"><</a>' +
                '<a class="next">></a>');

        var prev = $('.slider>.prev');
        var next = $('.slider>.next');

        $('.slider ul').each(function(){
            $(this).width(slideWidth * $(this).children('li').length);
        });

        prev.on('click', function(){
            var slider = $(this).parent();
            var currSlide = parseInt(slider.find('ul').data('current'));
            currSlide--;
            if (currSlide < 0) {
                currSlide = $(this).parent().find('ul li').size() - 1;
            }
            $(this).parent().find('ul').animate({
                left: -(currSlide * slideWidth)
            }, 300).data('current',currSlide);
        });
        next.on('click', function(){
            var slider = $(this).parent();
            var currSlide = parseInt(slider.find('ul').data('current'));
            if(++currSlide >= $(this).parent().find('ul li').size()) {
                currSlide = 0;
            }
            $(this).parent().find('ul').animate({
                left: -(currSlide * slideWidth)
            }, 300).data('current',currSlide);
        });
    }
});