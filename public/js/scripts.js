$(function(){
    var cache = {};
    var act;

    (function(){
        var url = location.href;
        var path = '/' + url.split('/')[3];
        $('.main-menu>li>a').each(function(){
            if ($(this).attr('href') === path) {
                $(this).parent().addClass('active');
            }
        });

    })();

    $('a.marker').hover(function(){
        var mark = parseInt($(this).data('val'));
        $('.marker').each(function(){
            if (mark >= $(this).data('val')) {
                $(this).addClass('hover');
            } else {
                $(this).removeClass('hover');
            }
        });
    }, function(){
        $('.marker').removeClass('hover');
    });

    $('#mark').on('click', 'a.marker', function(){
        var mark = parseInt($(this).data('val'));
        $('.sender').css('display', 'inline-block');
        $('.sender').data('val', mark);
        $('.marker').each(function(){
            if (mark >= $(this).data('val')) {
                $(this).addClass('check');
            } else {
                $(this).removeClass('check');
            }
        });
        return false;
    });
    $('#mark').on('click', 'a.sender', function(){
        var mark = $(this).data('val');
        if (/^[1-5]$/.test(mark)) {
            $.ajax({
                'url': location.href,
                'data': 'mark=' + mark,
                'type': 'GET',
                'success': function(responce){
                    $('.marker').css('display', 'none');
                    $('.sender').css('display', 'none');
                    var rating = parseFloat($('.rat').html());
                    var count = parseInt($('.num').html());
                    var result = (count * rating + mark) / (++count);
                    $('.rat').html(result.toFixed(2));
                    $('.num').html(count);
                }
            });
        }
        return false;
    });
    $('.main-menu a').on('click', (function(){
        var num = $(this).parent().data('num');
        if (!num) {
            return true;
        }
        if (num === act) {
            act = '';
            $(this).parent().children('ul').css('display', 'none');
        } else {
            act = num;
            $('.main-menu>li>ul').css('display', 'none');
            $(this).parent().children('ul').css('display', 'block');
        }
        return false;
    }));
});