
exports.index = function(req, res){
    res.render('main', {
        'title': 'Курсовая работа по Web-технологиям'
    });
};
exports.node = function(req, res){
	res.render('node', {
		'title': 'JavaScript in web'
	});
};
exports.ejs = function(req, res){
	res.render('ejs', {
		'title': 'JavaScript in web'
	});
};
exports.jade = function(req, res){
	res.render('jade', {
		'title': 'JavaScript in web'
	});
};
exports.smarty = function(req, res){
	res.render('smarty', {
		'title': 'JavaScript in web'
	});
};
exports.angular = function(req, res){
	res.render('angular', {
		'title': 'JavaScript in web'
	});
};
exports.backbone = function(req, res){
	res.render('backbone', {
		'title': 'JavaScript in web'
	});
};
exports.knockout = function(req, res){
	res.render('knockout', {
		'title': 'JavaScript in web'
	});
};
exports.jquery = function(req, res){
	res.render('jquery', {
		'title': 'JavaScript in web'
	});
};
exports.prototype = function(req, res){
	res.render('prototype', {
		'title': 'JavaScript in web'
	});
};
exports.rating = function(req, res){
    var fs = require('fs');
    var rating = 0;
    var count = 0;
    var val;
    if (/^[1-5]$/.test(req.query.mark)) {
        val = req.query.mark;
    }

    fs.readFile('bases/rating.md', 'utf-8', function(error, data){
        if (!error) {
            var arr = data.split(' ');
            rating = parseFloat(arr[0]);
            count = parseInt(arr[1]);

            if (!isNaN(val)) {
                count++;
                rating += +val;
                fs.writeFile('bases/rating.md',
                    rating.toString() + ' ' + count.toString()
                );
            }
        }
        if (count !== 0) {
            rating /= count;
        }
        res.render('rating', {
            'title': 'JavaScript in web',
            'rating': rating.toFixed(2),
            'count': count
        });
    });
};
