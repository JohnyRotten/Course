
/**
 * Module dependencies.
 */

var express = require('express');
var engine = require('ejs-locals');
var routes = require('./routes');
var http = require('http');
var path = require('path');

var app = express();

app.engine('ejs', engine);

// all environments
app.set('template_engine', 'ejs');
app.set('domain', 'localhost');
app.set('port', process.env.PORT || 80);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('view option', { layout: true });

// app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.json());
app.use(express.urlencoded());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/node', routes.node);
app.get('/template/ejs', routes.ejs);
app.get('/template/jade', routes.jade);
app.get('/template/smarty', routes.smarty);
app.get('/mvc/angular', routes.angular);
app.get('/mvc/backbone', routes.backbone);
app.get('/mvc/knockout', routes.knockout);
app.get('/others/jquery', routes.jquery);
app.get('/others/prototype', routes.prototype);
app.get('/rating', routes.rating);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
